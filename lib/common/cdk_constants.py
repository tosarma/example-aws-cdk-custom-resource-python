from aws_cdk import (
    Duration,
    aws_lambda,
    aws_logs,
)

LAMBDA_DEFAULTS :dict = {
    'RUNTIME': aws_lambda.Runtime.PYTHON_3_11,
    'MEMORY_SIZE': 128,
    'ARCHITECTURE': aws_lambda.Architecture.ARM_64,
    'CONCURRENCY': 2,
    'LOG_RETENTION': aws_logs.RetentionDays.ONE_WEEK,
    'TRACING': aws_lambda.Tracing.DISABLED,   ### Use OTEL instead.
    'TIMEOUT': Duration.seconds(300),

    'LAMBDA_INSIGHTS_VERSION': aws_lambda.LambdaInsightsVersion.VERSION_1_0_229_0,
    'LAMBDA_ADOT_CONFIG': aws_lambda.AdotInstrumentationConfig(
        layer_version=aws_lambda.AdotLayerVersion.from_python_sdk_layer_version(
             aws_lambda.AdotLambdaLayerPythonSdkVersion.LATEST,
        ),
        exec_wrapper=aws_lambda.AdotLambdaExecWrapper.INSTRUMENT_HANDLER,
    ),
}

### EoF
