import pathlib
import json
from datetime import datetime

from aws_cdk import (
    Duration,
    RemovalPolicy,
    Stack,
    aws_s3,
    aws_dynamodb,
    custom_resources as cr,
)
from constructs import Construct

from lib.common.cdk_constants import LAMBDA_DEFAULTS

THIS_DIR = str(pathlib.Path(__file__).parent)

### =======================================================================================

class MyBucketStack1 (Construct):
    """ Create just a bucket, and put a text-file (called 'aws_config.json') in it.
    """

    @property
    def bucket(self) -> aws_s3.Bucket:
        return self.__bucket

    @property
    def aws_config_filepath(self) -> str:
        return self.__aws_config_filepath

    ### -------------------------------------------------------
    def __init__(self, scope: Construct, construct_id: str,
                ENV :str,
                 **kwargs
    ) -> None:
        super().__init__(scope, construct_id, **kwargs)

        self.__bucket = aws_s3.Bucket(self, "dummy-bucket",
                bucket_name=Stack.of(self).account + f"-{ENV}-dummy-bucket",
                removal_policy=RemovalPolicy.DESTROY,
                auto_delete_objects=True,
                event_bridge_enabled=False,
                block_public_access=aws_s3.BlockPublicAccess.BLOCK_ALL,
                public_read_access=False,
                encryption=aws_s3.BucketEncryption.S3_MANAGED,
                enforce_ssl=True,
                transfer_acceleration=False,
                versioned=False,
                lifecycle_rules=[
                    aws_s3.LifecycleRule(
                        enabled=True,
                        expiration=Duration.days(7),
                        # transitions=[
                        #     aws_s3.
                        #     aws_s3.Transition(
                        #         storage_class=aws_s3.StorageClass.GLACIER,
                        #         transition_after=aws_s3.Duration.days(30),
                        #     ),
                        # ],
                    ),
                ]
        )

        aws_config_file_contents: str = json.dumps(
            {
                "Auth": {
                    "region": "us-east-1",
                    "identityPoolRegion": "us-east-1",
                    "userPoolId": "user_pool_id",
                    "userPoolWebClientId": "ui_client.user_pool_client_id",
                    "mandatorySignIn": False,
                    "identityPoolId": "identity_pool.identity_pool_id",
                    "domain": "hosted_ui_domain",
                    "oauth": {
                        "name": "Hosted UI",
                        "domain": "hosted_ui_domain",
                        "scope": ["email", "openid", "profile", "aws.cognito.signin.user.admin"],
                        "responseType": "code",
                    },
                },
                "group": "user_pool_group_name",
                "qs_url_lambda_name": "qs_url_lambda.function_name",
                "Storage": {
                    "AWSS3": {
                        "bucket": "my-bucket_name",
                        "region": "us-east-1",
                        "isObjectLockEnabled": True,
                    },
                },
                "ENV": ENV,
                "dynamo_table": "user.table_name",
                "my_lambda_name": "Undefined when initially CDK-deployed!",
            },
            indent=2,
        )

        self.__aws_config_filepath = f'{ENV}-data/aws_config.json'
        print( f"aws config file '{self.aws_config_filepath}': Contents= {aws_config_file_contents}" )

        ### TODO: remove this code below (to write to local-filesystem)
        pathlib.Path.mkdir( pathlib.Path(self.aws_config_filepath).parent, parents=True, exist_ok=True )  ### ensure a ./data folder exists on local-filesystem
        with open( self.aws_config_filepath, "w", encoding="utf-8" ) as f:
            f.write( aws_config_file_contents )
            f.close()

        ### ----------------------------------------------------------------
        putobj_aws_sdk_call = cr.AwsSdkCall(
            service='S3',
            action='putObject',         ### https://docs.aws.amazon.com/AmazonS3/latest/API/API_PutObject.html
            parameters={
                'Bucket': self.bucket.bucket_name,
                'Key': self.aws_config_filepath,
                'Body': aws_config_file_contents,   ### This is NOT an API-Param.  But, the payload to the API-call.
            },
            physical_resource_id=cr.PhysicalResourceId.of( self.aws_config_filepath +'.'+ str(datetime.now()) ),
        )

        removeObj_aws_sdk_call = cr.AwsSdkCall(
            service='S3',
            action='deleteObject',      ### https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObject.html
            parameters={
                'Bucket': self.bucket.bucket_name,
                'Key': self.aws_config_filepath,
            },
            physical_resource_id=cr.PhysicalResourceId.of( self.aws_config_filepath +'.'+ str(datetime.now()) ),
        )

        cr.AwsCustomResource( self, "aws-config",
            install_latest_aws_sdk=False,
            policy=cr.AwsCustomResourcePolicy.from_sdk_calls( resources=cr.AwsCustomResourcePolicy.ANY_RESOURCE ),
            log_retention=LAMBDA_DEFAULTS['LOG_RETENTION'],

            on_create = putobj_aws_sdk_call,
            on_update = putobj_aws_sdk_call,
            on_delete = removeObj_aws_sdk_call, ### deletes!!
        )

        ### ----------------------------------------------------------------
        aws_dynamodb.Table( scope=self, id="dummy-table",
            partition_key=aws_dynamodb.Attribute(   ### Required field!  Can _NOT_ be ignored/null/None
                name="id",
                type=aws_dynamodb.AttributeType.STRING,
            ),
            deletion_protection=False,
            removal_policy=RemovalPolicy.DESTROY,
            point_in_time_recovery=False,
        )

### EoF
