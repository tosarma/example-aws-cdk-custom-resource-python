from constructs import Construct
from aws_cdk import (
    Stack,
    aws_s3,
)

from lib.my_bucket_stack import MyBucketStack1
from lib.my_lambda_stack import MyLambdaStack
from lib.my_stack_delinker import MyStackDeLinker

class CdkCustomResourcePythonStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        ENV = self.node.try_get_context("ENV")

        stk1 = MyBucketStack1( self, ENV +"-Stk-s3PutObj",
                ENV=ENV,
        )

        stk22 = MyLambdaStack( self, construct_id = ENV +"-Stk-HelloWorldLambda",
                ENV=ENV,
                my_s3_bucket = stk1.bucket,
                aws_config_filepath = stk1.aws_config_filepath,
        )

        ### ------------------ Custom Resource ------------------
        MyStackDeLinker( self, construct_id = ENV +"-stack-delinker",
            ENV = ENV,
            my_s3_bucket = stk1.bucket,
            aws_config_filepath = stk1.aws_config_filepath,
            my_lambda = stk22.mylambda,
        )

### EoF
