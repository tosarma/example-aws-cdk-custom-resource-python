import pathlib
from datetime import datetime
from aws_cdk import (
    Stack,
    custom_resources as aws_cr,
    CustomResource,
    RemovalPolicy,
    aws_s3,
    aws_lambda,
    aws_lambda_python_alpha as python_lambda,
)

from constructs import Construct

### ----------------------------------------------------------------------------

from lib.common.cdk_constants import (
    LAMBDA_DEFAULTS
)

from lib.iamrole_lambda_custom_resource import gen_iam_role_for_CRlambda

THIS_DIR = pathlib.Path(__file__).parent

### ============================================================================

class MyStackDeLinker(Construct):

    def __init__( self, scope: "Construct", construct_id: str,
        ENV :str,
        my_s3_bucket: aws_s3.Bucket,
        aws_config_filepath :str,
        my_lambda: aws_lambda.Function,
    ) -> None:
        super().__init__(scope, construct_id)

        cr_id = ENV +"-cr-stack-delinker"

        role_cr_stack_delinker_handler = gen_iam_role_for_CRlambda( ref2self=self, scope=self,
            tier = ENV,
            cr_id = cr_id +"-role",
            iamrole_name = cr_id,
            s3_bucket = my_s3_bucket,
        )

        ### -------------------------------
        index = "custom_resource_handler.py"
        handler = "on_event"
        cr_stack_delinker_handler = python_lambda.PythonFunction(
            scope=self,
            id=cr_id +"-lambda-handler",
            function_name=cr_id,
            description=f"CDK-CustomResource - {cr_id}",
            entry=str(THIS_DIR.parent / 'src'),
            index=index,
            handler=handler,
            # environment={
            #     'Bucket': my_s3_bucket.bucket_name,
            #     'Key': aws_config_filepath,
            #     "my_lambda_name": my_lambda.function_name,
            #     "my_lambda_arn": my_lambda.function_arn,
            #     "physical_resource_id": ENV +'.'+ aws_config_filepath +'.'+ str(datetime.now()),
            #     # "physical_resource_id": aws_cr.PhysicalResourceId.of( ENV +'.'+ aws_config_filepath +'.'+ str(datetime.now()) ),
            # },
            # TODO: what is the right role to use?
            role = role_cr_stack_delinker_handler,
            runtime=LAMBDA_DEFAULTS['RUNTIME'],
            memory_size=LAMBDA_DEFAULTS['MEMORY_SIZE'],
            architecture=LAMBDA_DEFAULTS['ARCHITECTURE'],
            log_retention=LAMBDA_DEFAULTS['LOG_RETENTION'],
            tracing=LAMBDA_DEFAULTS['TRACING'],
            timeout=LAMBDA_DEFAULTS['TIMEOUT'],
            retry_attempts=0,
            reserved_concurrent_executions=LAMBDA_DEFAULTS['CONCURRENCY'],
            # adot_instrumentation=LAMBDA_DEFAULTS['LAMBDA_ADOT_CONFIG'],
            # insights_version=LAMBDA_DEFAULTS['LAMBDA_INSIGHTS_VERSION'],
        )

        props = {
            'ENV': ENV,
            'AWSRegion': Stack.of(self).region,
            'AWSAccount': Stack.of(self).account,
            'Bucket': my_s3_bucket.bucket_name,
            'Key': aws_config_filepath,
            "my_lambda_name": my_lambda.function_name,
            "my_lambda_arn": my_lambda.function_arn,
            "physical_resource_id": ENV +'.'+ aws_config_filepath +'.'+ str(datetime.now()),
            # "physical_resource_id": aws_cr.PhysicalResourceId.of( ENV +'.'+ aws_config_filepath +'.'+ str(datetime.now()) ),
        }

        self.customResourceProvider = aws_cr.Provider(self, construct_id +"-cr-provider",
            on_event_handler = cr_stack_delinker_handler,
            # is_complete_handler=is_complete,  # optional async "waiter"
                #### isComplete is needed when the lifecycle-operation(Create/Update/Delete).. ..
                ####    .. can __NOT__ be completed __immediately__ (FOR GOOD-REASONS).
                #### isComplete handler will be retried asynchronously after onEvent .. ..
                ####    .. (retried) until it returns `{ IsComplete: true }` .. or until it times out.
            log_retention = LAMBDA_DEFAULTS['LOG_RETENTION'],
            # role = ??,        ### must be assumable by the `lambda.amazonaws.com` service principal
        )

        self.resource = CustomResource( self, construct_id +"-cr-resource",
            service_token = self.customResourceProvider.service_token,
            properties = props,
            removal_policy = RemovalPolicy.DESTROY,
            resource_type = "Custom::"+ self.__class__.__name__,
        )

    ### ------------------------------------------
    # def create( self, props :dict ):  ### WARNING: this is handled __INSIDE__ the CR's lambda-handler.  _NOT_ here.
    #     pass

    # def update( self, props :dict ):  ### WARNING: this is handled __INSIDE__ the CR's lambda-handler.  _NOT_ here.
    #     pass

    # def delete( self, props :dict ):  ### WARNING: this is handled __INSIDE__ the CR's lambda-handler.  _NOT_ here.
    #     pass

### ----------------------------------------------------------------------------

### EoF
