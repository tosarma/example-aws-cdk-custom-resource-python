from aws_cdk import (
    Fn,
    aws_iam,
    aws_s3,
)

from constructs import Construct

### ----------------------------------------------------------------------------

### Global constants used ONLY in this file.

# std_lambda_aws_managed_policy = "AWSLambdaExecute"
std_lambda_aws_managed_policy = "AWSLambdaBasicExecutionRole"
arn_std_lambda_aws_managed_policy = f"arn:aws:iam::aws:policy/service-role/{std_lambda_aws_managed_policy}"

lambda_cr_service_fqdn   = "lambda.amazonaws.com"

### ----------------------------------------------------------------------------

def gen_iam_role_for_CRlambda ( ref2self :Construct, scope: "Construct",
            tier :str,                  ### Mandatory param
            cr_id :str,                 ### Mandatory param
            iamrole_name :str,          ### Mandatory param
            s3_bucket :aws_s3.Bucket,   ### Unique to this need
) -> aws_iam.IRole:
    """ Attention: This is a Custom-Resource !!!
        A LAMBDA handles all of that Custom-Resource's CREATE/UPDATE/DESTROY  events.
        This IAM-Role provides ALL ACCESS that this LAMBDA needs.

    Args:
        ref2self (Construct): reference to `self` object from caller.
        scope (Construct): the `scope` variable in caller
        tier (str): dev|test|stage|prod
        cr_id (str): the Custom-Resource's ID (to be used to NAME the CDK-constructs)
        iamrole_name (str): the IAM-Role's name (to be used to NAME the CDK-constructs)

        s3_bucket (aws_s3.Bucket): the S3-Bucket that's accessed by the Lambda.
    """

    secret_name_patt = f"{tier}/cr-lambda/*"

    ### -----------------------------------------------------------------------------

    std_mgd_policy = aws_iam.ManagedPolicy.from_managed_policy_arn(
        scope = scope,
        id=cr_id +"-std-aws-mgd-lambda-policy",
        managed_policy_arn = arn_std_lambda_aws_managed_policy
    )

    lambda_cr_policystmt_secrets = aws_iam.PolicyStatement(
        sid="CFTCusResServiceRoleSecretsMgr",
        actions=[ "secretsmanager:GetSecretValue", ],
        effect  = aws_iam.Effect.ALLOW,
        resources=[ Fn.sub( "arn:${AWS::Partition}:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:"+ secret_name_patt ), ],
        # resources=[ Fn.sub( "arn:${AWS::Partition}:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:"+IMS_SEER_API_secret[p_or_not_p]+"*" ), ],
        ### IAM-Role's example: resources = [ Arn.format( ArnComponents( service="s3", resource="role", resource_name="my-own-iam-rolename", arn_format=ArnFormat.SLASH_RESOURCE_NAME ), stack=ref2self.stack ) ],
    )

    lambda_cr_policy = aws_iam.PolicyDocument(          ### https://docs.aws.amazon.com/cdk/api/v2/python/aws_cdk.aws_iam/PolicyDocument.html#aws_cdk.aws_iam.PolicyDocument
        statements= [
            lambda_cr_policystmt_secrets,
            # lambda_cr_policystmt_all,
        ],
    );
    lambda_cr_policy.validate_for_resource_policy();

    ### ----------------
    ### create IAM-Role for aws_custom-resource
    lambda_cr_role :aws_iam.IRole = aws_iam.Role( scope, "aws_cr-role",
        role_name = iamrole_name,
        description = f"{tier}-tier IAM-Role for CUSTOM-RESOURCE {cr_id}",
        inline_policies= { iamrole_name: lambda_cr_policy, },
        managed_policies=[ std_mgd_policy ],
        assumed_by = aws_iam.ServicePrincipal( lambda_cr_service_fqdn ),
    );
    s3_bucket.grant_read_write( lambda_cr_role )

    ### ----------------
    ref2self.crRole = lambda_cr_role;

    return lambda_cr_role

### ----------------------------------------------------------------------------

### EoF
