import pathlib

from aws_cdk import (
    aws_iam,
    aws_s3 as _s3,
    aws_lambda_python_alpha as python_lambda,
    aws_lambda,
    aws_dynamodb as dynamodb,
)

from constructs import Construct

from lib.common.cdk_constants import LAMBDA_DEFAULTS

from lib.my_stack_delinker import MyStackDeLinker

THIS_DIR = pathlib.Path(__file__).parent

### ===========================================================================================

class MyLambdaStack(Construct):

    @property
    def mylambda(self) -> aws_lambda.Function:
        return self.__my_lambda

    ### ---------------------------------------------------------
    def __init__( self, scope: "Construct", construct_id: str,
        ENV :str,
        my_s3_bucket: _s3.Bucket,
        aws_config_filepath :str,
    ) -> None:
        super().__init__(scope, construct_id)

        my_lambda_role = aws_iam.Role(self, ENV +"-role-mylambda",
                assumed_by=aws_iam.ServicePrincipal("lambda.amazonaws.com"),
                managed_policies=[
                    aws_iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole"),
                    # aws_iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaVPCAccessExecutionRole"),
                ],
        )

        self.__my_lambda = aws_lambda.Function( self, ENV +"-mylambda",
                code=aws_lambda.Code.from_asset( str( THIS_DIR.parent / "src") ),
                handler="hello_world.handler",
                description=f"{ENV}: a very-basic PYTHON hello-world lambda",
                role=my_lambda_role,
                runtime=LAMBDA_DEFAULTS['RUNTIME'],
                memory_size=LAMBDA_DEFAULTS['MEMORY_SIZE'],
                architecture=LAMBDA_DEFAULTS['ARCHITECTURE'],
                log_retention=LAMBDA_DEFAULTS['LOG_RETENTION'],
                tracing=LAMBDA_DEFAULTS['TRACING'],
                timeout=LAMBDA_DEFAULTS['TIMEOUT'],
                retry_attempts=0,
                reserved_concurrent_executions=LAMBDA_DEFAULTS['CONCURRENCY'],
                # adot_instrumentation=LAMBDA_DEFAULTS['LAMBDA_ADOT_CONFIG'],
                # insights_version=LAMBDA_DEFAULTS['LAMBDA_INSIGHTS_VERSION'],
        )
        my_s3_bucket.grant_read(self.mylambda)

        my_lambda_role.node.add_dependency(my_s3_bucket) ### Was getting race-condition error (that Bucket was Not yet created!!)


### EoF
