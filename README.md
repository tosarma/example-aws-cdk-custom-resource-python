# First things first

Read [parent readme](../README-Python.md)

<HR>
<HR>
<HR>
<HR>
<HR>

# INSIGHTS

1.  Lambda source-code is under `./src` subfolder.
1.  One Lambda [Custom-Resource-𝛌](./src/custom_resource_handler.py) handles the CREATE/UPDATE/DELETE events for **CUSTOM-RESOURCE**
    *  The Custom-Resource is defined inside [my_stack_delinker.py](./lib/my_stack_delinker.py)
1.  ATTENTION: The CR-𝛌 requires its own [`requirements.txt`](./src/requirements.txt) that is present in the `.src/` sub-folder (Warning: Do __ NOT __ use `requirements.in`)
1.  NOTE: The CR-𝛌 requires its own IAM-Role.<BR/>In this implemented-example: the IAM-Role has unique-requirements to allow S3-access and SecretsMgr.<BR/>Make sure to customize this IAM-Role, so that that the CR can do what it's supposed to do.


<HR>
<HR>
<HR>
<HR>
<HR>

# EoF
