#!/usr/bin/env python3

import aws_cdk as cdk

from lib.cdk_custom_resource_python_stack import CdkCustomResourcePythonStack


app = cdk.App()
CdkCustomResourcePythonStack(app, "CRTestPy")

app.synth()
