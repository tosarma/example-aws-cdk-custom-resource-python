import os
import sys
import pathlib
import boto3
import time

DEBUG = True

class MyException(Exception):
    pass

### --------------------------------------------------------------------------------------------
## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### --------------------------------------------------------------------------------------------

cwd_contents = pathlib.Path.cwd().glob( "*" )   ### <------------------------- All FOLDERS + FILES in current folder

### NOTE: Keep following as RELATIVE_FILEPATHs ONLY.
### WARNING: do __NOT__ use ABSOLUTE_FILEPATHs.
LIST_of_LOCAL_PATHS = (
    # str( pathlib.Path( ".github" ) ),
    # str( pathlib.Path( "folder1" ) ),
    # str( pathlib.Path( "folder2" ) ),
    # str( pathlib.Path( "folder3/subfldr3" ) ),
    # str( pathlib.Path( "folder4/common.py" ) ),
    # str( pathlib.Path( "folder4/sf4/3deep/myscript.py" ) ),
    # str( pathlib.Path( "constants.py" ) ),
    str( pathlib.Path( "requirements.in" ) ),
    str( pathlib.Path( "requirements.txt" ) ),
    # str( pathlib.Path( "package.json" ) ),
    # str( pathlib.Path( "package-lock.json" ) ),
)

### -----------------------------------------------------------------------------

def cleanup_bucket(bucket_name: str, prefix: str) -> None:
    """ Destroys everything UNDER a specific PREFIX of S3 bucket.   """
    s3 = boto3.resource('s3')
    # List all objects in the bucket with the specified prefix
    bucket = s3.Bucket(bucket_name)
    objects = bucket.objects.filter(Prefix=prefix)
    # Delete each object
    for obj in objects:
        obj.delete()

### -----------------------------------------------------------------------------

def checkIfSingleS3ObjectExists(bucket_name: str, s3_prefix: str) -> bool:
    """ Exactly one match to the Prefix is expected.
        Example: if you provide a "folder-prefix" (that has MULTIPLE objects under it), then this function returns False!!!
    """
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    objs = list(bucket.objects.filter(Prefix=s3_prefix))
    if DEBUG: print( f"S3-API returned the following - checkIfSingleS3ObjectExists(): "+ __file__ )
    if DEBUG: print( objs )
    return len(objs) == 1 and objs[0].key == s3_prefix
    # return len(objs) > 0 and objs[0].key == s3_prefix

### -----------------------------------------------------------------------------

def checkIfS3PrefixHasObjects(bucket_name: str, s3_prefix: str) -> int:
    """ HOW-MANY object(s) are present under the Prefix?
        returns an INTEGER.  'zero' means no objects found.
    """
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    objs = list(bucket.objects.filter(Prefix=s3_prefix))
    return len(objs)
    # return len(objs) > 0 and objs[0].key == s3_prefix

### -----------------------------------------------------------------------------

def downloadFileFromS3(bucketFrom :str, s3_prefix :str, folderPathTo :str) -> pathlib.Path:
    """ Note: returns a Pathlib.Path for downloaded file. _NOT_ the contents of downloaded-file
    """
    if not pathlib.Path(folderPathTo).exists() and not pathlib.Path(folderPathTo).is_dir():
        raise MyException(f"ERROR: folderPathTo '{folderPathTo}' is NOT a directory/folder.")
    ### pathlib.Path.mkdir(folderPathTo, parents=True, exist_ok=True )  <-- in case folderPathTo is a file-path (mistakenly by developer)
    s3 = boto3.client('s3')
    local_path = folderPathTo +'/'+ pathlib.Path(s3_prefix).name
    s3.download_file( bucketFrom, s3_prefix, local_path )
    if DEBUG: print( f"S3-download Successful to {local_path} at {pathlib.Path.cwd()} - downloadFileFromS3(): "+ __file__ )
    return pathlib.Path(local_path).resolve().absolute()

### -----------------------------------------------------------------------------

def uploadTextToS3(bucketTo: str, s3_prefix: str, text_content: str):
    """ Attention: The 3rd parameter is INLINE textual-content.  __NOT__ the local-filepath!!!
    """
    s3 = boto3.resource('s3')
    s3.Object(bucketTo, s3_prefix).put(Body=text_content)
    if DEBUG: print( "S3-Upload Successful - uploadTextToS3(): "+ __file__ )
    return True

### -----------------------------------------------------------------------------

def copyFolderFromS3(pathFrom, bucketTo, locationTo):
    response = {}
    response['status'] = 'failed'
    getBucket = pathFrom.split('/')[2]
    location = '/'.join(pathFrom.split('/')[3:])
    if pathFrom.startswith('s3://'):
        copy_source = { 'Bucket': getBucket, 'Key': location }
        uploadKey = locationTo
        recursiveCopyFolderToS3(copy_source,bucketTo,uploadKey)

### ---------------------------------------------------------------------
def recursiveCopyFolderToS3(src,uplB,uplK):
    more_objects=True
    found_token = True
    while more_objects:
        if found_token:
            response = s3.list_objects_v2(
                Bucket=src['Bucket'],
                Prefix=src['Key'],
                Delimiter="/")
        else:
            response = s3.list_objects_v2(
                Bucket=src['Bucket'],
                ContinuationToken=found_token,
                Prefix=src['Key'],
                Delimiter="/")
        for source in response["Contents"]:
            raw_name = source["Key"].split("/")[-1]
            raw_name = raw_name
            new_name = os.path.join(uplK,raw_name)
            if raw_name.endswith('_$folder$'):
                src["Key"] = source["Key"].replace('_$folder$','/')
                new_name = new_name.replace('_$folder$','')
                recursiveCopyFolderToS3(src,uplB,new_name)
            else:
                src['Key'] = source["Key"]
                s3.copy_object(CopySource=src,Bucket=uplB,Key=new_name)
                if "NextContinuationToken" in response:
                    found_token = response["NextContinuationToken"]
                    more_objects = True
                else:
                    more_objects = False

### --------------------------------------------------------------------------------------------
## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### --------------------------------------------------------------------------------------------

import s3fs         ### 3rd party. But popular.  How much can we trust it?
                    ### 3rd party. But popular.  How much can we trust it?
                    ### 3rd party. But popular.  How much can we trust it?

### --------------------------------------------------------------------------------------------

def s3fs_recursive_cp( a_local_path :str, dest_path :str, max_depth :int = 7 ) -> None:
    """
        Copies _ONE_ SINGLE file/folder ONLY.
        Requires 3rd party module called s3fs (pip3 install s3fs).
    """
    ### https://s3fs.readthedocs.io/en/latest/#examples
    tf = s3fs.S3FileSystem(
        anon=False,   ### uses AWS-default credentials/profile
        s3_additional_kwargs={'ServerSideEncryption': 'AES256'}
        ### https://s3fs.readthedocs.io/en/latest/#serverside-encryption
    )

    src_fldr = '/'.join( a_local_path.split('/')[0:-1] )
    ### enh_dest_path helps to replicate the source-folder-structure in s3-bucket
    if src_fldr == '':
        enh_dest_path = dest_path  ### implies a _SIMPLE_ file-name/folder-name only
    else:
        enh_dest_path = dest_path +'/'+ src_fldr
    if  not   enh_dest_path.endswith('/'):  ### s3fs.put() requires trailing '/'
        enh_dest_path += '/'
    if DEBUG: print( f"copying(recursively) from {a_local_path} -to-> {enh_dest_path} .. ",
            end="\n\n", flush=True )

    tf.put( a_local_path, enh_dest_path, recursive=True, maxdepth=max_depth, )
    ### https://s3fs.readthedocs.io/en/latest/api.html#s3fs.core.S3FileSystem.put

### -----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------

def copy_list_to_bucket( local_paths :list,
                bucket_name :str,
                bucket_prefix :str ) -> None:
    """ recursively copies a __LIST__ of FILE/FOLDER paths (passed in as 1st param),
        to the S3 bucket specified by 2nd & 3rd params.
    """

    dest_path = bucket_name +'/'+ bucket_prefix
    if DEBUG: print( f"S3 'dest_path' (for Glue-Scripts stored in Git) = '{dest_path}'")

    for a_local_path in local_paths:
        if DEBUG: print( f"a_local_path: '{a_local_path}'" )
        s3fs_recursive_cp( a_local_path, dest_path )

    if DEBUG: print( f"Successfully copied _MULTIPLE_ files/folders recursively to S3-bucket {dest_path}.")

### --------------------------------------------------------------------------------------------
## @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### --------------------------------------------------------------------------------------------

if __name__ == "__main__":

    if DEBUG: print( f"len(sys.argv): {len(sys.argv)} inside file: {__file__}" )
    if len(sys.argv) != 4:
        if DEBUG: print( "\n!! ERROR !! missing argument <bucketname> <bucketPREFIX>  <topmost_path>" )
        if DEBUG: print( f"Usage: export PYTHONPATH=$(pwd);     {sys.argv[0]}  <bucketname> <bucketPREFIX>  <topmost_path>" )
        if DEBUG: print( f"Usage: export PYTHONPATH=$(pwd);     {sys.argv[0]}  generic-bucket-591580567012  python-test  UNUSED-PATH " )
        sys.exit(1)

    if DEBUG: print( f"Running script {sys.argv[0]} .. using DEFAULT AWS_PROFILE .." )

    _glue_script_bucketname     = sys.argv[1]
    _glue_script_bucket_prefix  = sys.argv[2]
    topmost_path                = sys.argv[3]
    if DEBUG: print( f"topmost_path (as-is) = '{topmost_path}'" )

    if DEBUG: print( "cwd_contents: " );
    if DEBUG: print( cwd_contents )

    if DEBUG: print( f"\n\n Cleaning out ENTIRE bucket {_glue_script_bucketname} -- EVERYTHING UNDER PREFIX='{_glue_script_bucket_prefix}'" );
    cleanup_bucket(
        bucket_name = _glue_script_bucketname,
        prefix = _glue_script_bucket_prefix,
    )

    if DEBUG: print( f"\n\n Copying LIST-of-FILES into bucket {_glue_script_bucketname}" );
    copy_list_to_bucket(
        local_paths = LIST_of_LOCAL_PATHS,
        bucket_name = _glue_script_bucketname,
        bucket_prefix = _glue_script_bucket_prefix,
    )

    s3_prefix=_glue_script_bucket_prefix +'/'+ 'dynamically-stored-text-content.txt'
    if DEBUG: print( f"\n\n UPLOADING text-string into bucket {_glue_script_bucketname} at PREFIX='{s3_prefix}'" );
    uploadTextToS3(
        text_content="ABC-123 .. " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()),
        bucketTo=_glue_script_bucketname,
        s3_prefix=s3_prefix,
    )

    if DEBUG: print( f"\n\n DOWN-load from bucket {_glue_script_bucketname} from PREFIX='{s3_prefix}'" );
    pth = downloadFileFromS3(
        bucketFrom=_glue_script_bucketname,
        s3_prefix=s3_prefix,
        folderPathTo='/tmp/', ### Warning: Folder-path.  NOT a file-path.
    )
    print( pth )

### EoF
