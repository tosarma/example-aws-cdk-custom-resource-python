### WARNING!!
### Lambda that handles Custom-Resource's CREATE/UPDATE/DESTROY events.

import pathlib
from datetime import datetime
import json
import boto3

from s3_utils import (  ### This is a local file in the same directory as this file.
    downloadFileFromS3,
    uploadTextToS3,
    cleanup_bucket,
)

### -----------------------------------------------------------------------------

DEBUG = True

class MyException(Exception):
    pass

### -----------------------------------------------------------------------------
### @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### -----------------------------------------------------------------------------

def common_code( s3bucketname :str, s3objprefix :str, my_lambda_name :str, my_lambda_arn :str ):
    """ UNIQUE-REQUIREMENT: download a file from an s3-bucket.  It should be JSON. Append an element to JSON. Write it back as the same s3-object.
    """
    pth = downloadFileFromS3( bucketFrom=s3bucketname, s3_prefix=s3objprefix, folderPathTo="/tmp/" )
    print( f"Downloaded file '{pth}' from S3-bucket '{s3bucketname}'" )
    with open( pth, "r", encoding="utf-8" ) as f:
        s = f.read()
        print ( s )
        f.close()
        json_data = json.loads(s)

    print( json_data )
    json_data["my_lambda_name"] = my_lambda_name
    json_data["my_lambda_arn"] = my_lambda_arn
    print( json_data )
    uploadTextToS3( bucketTo=s3bucketname, s3_prefix=s3objprefix, text_content=json.dumps(json_data) )


### -----------------------------------------------------------------------------

def on_event(event, context):
    print(event)
    request_type = event['RequestType']
    if request_type == 'Create': return on_create(event)
    if request_type == 'Update': return on_update(event)
    if request_type == 'Delete': return on_delete(event)
    raise Exception("Invalid request type: %s" % request_type)

def on_create(event):
    print("ON-CREATE resource: in "+ __file__ )
    print( event )
    props = event["ResourceProperties"]
    print( props )

    ENV = props["ENV"]
    s3bucketname = props["Bucket"]
    s3objprefix = props["Key"]

    physical_id = ENV +'.'+ s3bucketname +'.'+ s3objprefix +'.'+ str(datetime.now())
    print( f"DEFINED the Physical-ID to be -> '{physical_id}' (just created)" )

    common_code( s3bucketname=s3bucketname,
                s3objprefix=s3objprefix,
                my_lambda_name=props["my_lambda_name"],
                my_lambda_arn=props["my_lambda_arn"],
    )
    return { 'PhysicalResourceId': physical_id }

def on_update(event):
    print("UPDATE new resource: in "+ __file__ )
    print( event )
    props = event["ResourceProperties"]
    print( props )

    ENV = props["ENV"]
    s3bucketname = props["Bucket"]
    s3objprefix = props["Key"]
    physical_id = event["PhysicalResourceId"]
    print( f"Physical-ID='{physical_id}'" )

    common_code( s3bucketname=s3bucketname,
                s3objprefix=s3objprefix,
                my_lambda_name=props["my_lambda_name"],
                my_lambda_arn=props["my_lambda_arn"],
    )

def on_delete(event):
    print("Destroy/Delete resource: in "+ __file__ )
    print( event )
    props = event["ResourceProperties"]
    print( props )

    ENV = props["ENV"]
    s3bucketname = props["Bucket"]
    s3objprefix = props["Key"]
    physical_id = event["PhysicalResourceId"]
    print( f"Physical-ID='{physical_id}'" )

    cleanup_bucket( bucket_name=s3bucketname, prefix=s3objprefix )

### -----------------------------------------------------------------------------
### [provider-framework] PAYLOAD for executing user function arn:aws:lambda:us-east-2:123456789012:function:sandbox-cr-stack-delinker

{
    "RequestType": "Create",   ### <---------------------
    "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",
    "ResponseURL": "https://cloudformation-custom-resource-response-useast2.s3.us-east-2.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-2%3A123456789012%3Astack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab%7Csandboxstackdelinkersandboxstackdelinkercrresource97A4D813%7C096724dc-57b2-4d97-abe6-9edee5ac6773?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240117T013307Z&X-Amz-SignedHeaders=host&X-Amz-Expires=7200&X-Amz-Credential=AKIAVRFIPK6PAHZKUES4%2F20240117%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Signature=1150ad6f6db3dfef3df3e56ad0af379ad2e8e7e6a5bdfa47b32fadabc6b6c45a",
    "StackId": "arn:aws:cloudformation:us-east-2:123456789012:stack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab",
    "RequestId": "096724dc-57b2-4d97-abe6-9edee5ac6773",  ### Unique Unique Unique
    "LogicalResourceId": "sandboxstackdelinkersandboxstackdelinkercrresource97A4D813",
    "ResourceType": "Custom::MyStackDeLinker",
    "ResourceProperties": {   ### This is my custom properties.  All of the above are defined by AWS.
        "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",
        "my_lambda_name": "CRTestPy-sandboxStkHelloWorldLambdasandboxmylambda-WfU0AtV6NeGb",
        "Bucket": "123456789012-sandbox-dummy-bucket",
        "Key": "sandbox-data/aws_config.json",
        "AWSAccount": "123456789012",
        "AWSRegion": "us-east-2",
        "ENV": "sandbox",
        "physical_resource_id": "sandbox.sandbox-data/aws_config.json.2024-01-16 20:30:40.740807",
    }
}


{
    "RequestType": "Update",   ### <---------------------
    "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",   ### SAME !
    "ResponseURL": "https://cloudformation-custom-resource-response-useast2.s3.us-east-2.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-2%3A123456789012%3Astack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab%7Csandboxstackdelinkersandboxstackdelinkercrresource97A4D813%7Cd9d02a47-61ce-44b1-8350-c18a5beb5cf3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240117T014400Z&X-Amz-SignedHeaders=host&X-Amz-Expires=7200&X-Amz-Credential=AKIAVRFIPK6PAHZKUES4%2F20240117%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Signature=7472fb1f4fe3677b16f594b061f0f241b3c2f8038f6084d65b8a37c531dba144",
    "StackId": "arn:aws:cloudformation:us-east-2:123456789012:stack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab",   ### SAME !
    "RequestId": "d9d02a47-61ce-44b1-8350-c18a5beb5cf3",  ### Unique Unique Unique
    "LogicalResourceId": "sandboxstackdelinkersandboxstackdelinkercrresource97A4D813",  ### SAME !
    "PhysicalResourceId": "sandbox.123456789012-sandbox-dummy-bucket.sandbox-data/aws_config.json.2024-01-17 01:33:09.973347", ### Same for BOTH UPDATE & DELETE
    "ResourceType": "Custom::MyStackDeLinker",  ### SAME !
    "ResourceProperties": {
        "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",
        "my_lambda_name": "CRTestPy-sandboxStkHelloWorldLambdasandboxmylambda-WfU0AtV6NeGb",  ### SAME !
        "AWSAccount": "123456789012",
        "Bucket": "123456789012-sandbox-dummy-bucket",
        "AWSRegion": "us-east-2",
        "ENV": "sandbox",
        "physical_resource_id": "sandbox.sandbox-data/aws_config.json.2024-01-16 20:43:11.456834",   ### Same for BOTH UPDATE & DELETE
        "Key": "sandbox-data/aws_config.json"
    },
    "OldResourceProperties": {   ### Unique Unique Unique -- only for UPDATE !!!
        "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",  ### SAME !
        "my_lambda_name": "CRTestPy-sandboxStkHelloWorldLambdasandboxmylambda-WfU0AtV6NeGb",
        "Bucket": "123456789012-sandbox-dummy-bucket",
        "Key": "sandbox-data/aws_config.json",
        "AWSAccount": "123456789012",
        "AWSRegion": "us-east-2",
        "ENV": "sandbox",
        "physical_resource_id": "sandbox.sandbox-data/aws_config.json.2024-01-16 20:30:40.740807",  ### OLD-VALUE. Matches CREATE ONLY!!!
    }
}


{
    "RequestType": "Delete",  ### <---------------------
    "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",
    "ResponseURL": "https://cloudformation-custom-resource-response-useast2.s3.us-east-2.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-2%3A123456789012%3Astack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab%7Csandboxstackdelinkersandboxstackdelinkercrresource97A4D813%7Ca9a08f08-8e0f-45ac-b31d-d048997bcbe9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240117T015142Z&X-Amz-SignedHeaders=host&X-Amz-Expires=7200&X-Amz-Credential=AKIAVRFIPK6PAHZKUES4%2F20240117%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Signature=aaa797ee317d79b980d14ed1a658fc6a19e8668d1fd75ab35b73602c0808e62e",
    "StackId": "arn:aws:cloudformation:us-east-2:123456789012:stack/CRTestPy/21889980-b4d8-11ee-a7e4-02875404b2ab",
    "RequestId": "a9a08f08-8e0f-45ac-b31d-d048997bcbe9",
    "PhysicalResourceId": "sandbox.123456789012-sandbox-dummy-bucket.sandbox-data/aws_config.json.2024-01-17 01:33:09.973347", ### Same for BOTH UPDATE & DELETE
    "LogicalResourceId": "sandboxstackdelinkersandboxstackdelinkercrresource97A4D813", ### SAME
    "ResourceType": "Custom::MyStackDeLinker",  ### SAME
    "ResourceProperties": { ### __NOT__ same even tho' I pass this in.
        "ServiceToken": "arn:aws:lambda:us-east-2:123456789012:function:CRTestPy-sandboxstackdelinkersandboxstackdelinkerc-fVAfm4WaUBao",
        "my_lambda_name": "CRTestPy-sandboxStkHelloWorldLambdasandboxmylambda-WfU0AtV6NeGb",
        "Bucket": "123456789012-sandbox-dummy-bucket",
        "Key": "sandbox-data/aws_config.json",
        "AWSAccount": "123456789012",
        "AWSRegion": "us-east-2",
        "ENV": "sandbox",
        "physical_resource_id": "sandbox.sandbox-data/aws_config.json.2024-01-16 20:43:11.456834", ### Same for BOTH UPDATE & DELETE
    }
}

### EoF
